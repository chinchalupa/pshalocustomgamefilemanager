# PSHaloCustomGameFileManager
This is a simple Powershell tool for the management and mapping of game and map variants in the Halo Master Chief Collection.

## Motivation
The Halo custom game browser does not lend itself to easy navigation. It has a linear scroll with a max of 4 items visible at a time which makes finding game types and maps rather difficult when you have 300+ maps and game types. This module provides a command-line solution for loading matching game types and maps to the Halo custom games directories. Using this module, game types and maps can be swapped in at a moments notice meaning the number of maps to iterate is reduced drastically. This means less time scrolling through menus and more time playing custom games!

## Setup Instructions
Download the megapack of halo customs which can be found here: https://halocustoms.com/maps/halocustoms-megapack.2261/
Make note of your installation directory as you will use this for the command line tools.

Local Instructions:
Clone the repository. Then, import the module from the install directory via: `Import-Module -Path .\src\PSHaloCustomGameFileManager\PSHaloCustomGameFileManager.psd1`

## Functions

#### Get-AvailableGameSet

Returns the list of available game sets.

```powershell
> Get-AvailableGameSet

Jenga
Indiana Jones
```

Returns a specified game set.

```powershell
> Get-AvailableGameSet -GameSet 'Jenga'
```

#### Install-HaloCustomGameSet

Installs the specified game and map variants from the source directory to the "Halo Master Chief Collection" folder.

```powershell
> $haloRootDirectory = "<your steam directory>\steamapps\common\Halo The Master Chief Collection"
> $sourceDirectory = "<your install directory here>\HaloCustoms_Megapack_HaloReach_MASTER_V3.0\HaloCustoms_Megapack_HaloReach_MASTER_V3.0"
> Install-HaloCustomGameSet -HaloRootDirectory $haloRootDirectory -SourceDirectory $sourceDirectory -GameSet 'Jenga'
```

#### Uninstall-HaloCustomGameSet

Uninstalls all currently installed map and game variants from the "Halo Master Chief Collection" folder.

```powershell
> $haloRootDirectory = "<your steam directory>\steamapps\common\Halo The Master Chief Collection"
> $sourceDirectory = "<your install directory here>\HaloCustoms_Megapack_HaloReach_MASTER_V3.0\HaloCustoms_Megapack_HaloReach_MASTER_V3.0"
> Install-HaloCustomGameSet -HaloRootDirectory $haloRootDirectory -SourceDirectory $sourceDirectory -GameSet 'Jenga'
```

#### Initialize-HaloCustomGameSet

Uninstalls all currently installed custom game sets and then installs the specified game sets.

```powershell
> $haloRootDirectory = "<your steam directory>\steamapps\common\Halo The Master Chief Collection"
> $sourceDirectory = "<your install directory here>\HaloCustoms_Megapack_HaloReach_MASTER_V3.0\HaloCustoms_Megapack_HaloReach_MASTER_V3.0"
> Install-HaloCustomGameSet -HaloRootDirectory $haloRootDirectory -SourceDirectory $sourceDirectory -GameSet 'Jenga'
```
