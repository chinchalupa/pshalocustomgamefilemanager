<#
.SYNOPSIS
Returns all the game sets as a list of objects.

.EXAMPLE
> Get-HaloCustomGameMappingObject

Returns the game sets from the default json file 'gameset_custom_game_map.json'.

.EXAMPLE
> Get-HaloCustomGameMappingObject -MappingFile $mappingFile

Returns the game sets from the supplied mapping file. File must contain a
'GameSets' object at the root of the json formatted file.
#>
function Get-HaloCustomGameMappingObject {
    [CmdletBinding()]
    param (
        [Parameter()]
        [ValidateScript({ $_ | Test-Path -and $_.Extension -eq 'json' })]
        [System.IO.FileInfo]
        $MappingFile = $(Get-Item $PSScriptRoot\..\resource\gameset_custom_game_map.json)
    )
    PROCESS {
        (Get-Content $MappingFile -Raw | ConvertFrom-Json).GameSets
    }
}
