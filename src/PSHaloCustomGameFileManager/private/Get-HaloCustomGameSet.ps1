<#
.SYNOPSIS
Retrieves the specific game sets from the custom game mapping object.

.PARAMETER CustomGameMappingObject
The custom mapping object used to map matching game and map variants.

.PARAMETER GameSet 
The custom game set to retrieve.

.EXAMPLE
> Get-HaloCustomGameSet

Returns all the Halo custom game sets for every Halo game available.

.EXAMPLE
> Get-HaloCustomGameSet -GameSet 'Jenga'

Returns the gameset matching the name 'Jenga' for every Halo game available.

.EXAMPLE
> Get-HaloCustomGameSet -GameSet 'Jenga' -CustomGameMappingObject $customMapping

Searches the custom mapping $customMapping to find the game set named 'Jenga'.
#>
function Get-HaloCustomGameSet {
    [CmdletBinding()]
    param (
        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [PSCustomObject]
        $CustomGameMappingObject = (Get-HaloCustomGameMappingObject),

        [Parameter(ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [ValidateNotNullOrEmpty()]
        [String]
        $GameSet
    )
    PROCESS {
        $CustomGameMappingObject | Where-Object Name -EQ $GameSet | ForEach-Object {
            Write-Verbose "Converting $($_.Name) to Halo custom game object."
            [HaloCustomGameSet]$_
        }
    }
}
