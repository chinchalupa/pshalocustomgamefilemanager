<#
.SYNOPSIS
Synchronizes the requested game sets in the source directory to the root
directory.

.DESCRIPTION
Synchronizes the requested gamesets in the source directory with those in the
root directory. Utilizes a soft symbolic link rather than copying the files to
avoid duplication.

.PARAMETER GameSets
The game set to clone.

.PARAMETER HaloRootDirectory
The root directory to create symlinks to.

.PARAMETER SourceDirectory
The source directory to pull the map/game variants from.

.EXAMPLE
> $haloRootDirectory = "D:\SteamLibrary\steamapps\common\Halo The Master Chief Collection"
> $sourceDirectory = "<your install directory here>"
> Sync-HaloCustomGameDirectory -HaloRootDirectory $haloRootDirectory -SourceDirectory $sourceDirectory

Synchronizes the halo game and map variant directories provided with all game
sets.
#>
function Sync-HaloCustomGameDirectory {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        [Parameter(ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [ValidateNotNullOrEmpty()]
        [HaloCustomGameSet]
        $GameSets = (Get-HaloCustomGameSet),

        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [ValidateScript({ $_ | Test-Path })]
        [System.IO.DirectoryInfo]
        $HaloRootDirectory,

        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [ValidateScript({ $_ | Test-Path })]
        [System.IO.DirectoryInfo]
        $SourceDirectory
    )
    PROCESS {
        foreach ( $gameset in $Gamesets ) {
            $gameDirectory = Join-Path -Path $HaloRootDirectory -ChildPath $gameSet.halo_game

            Write-Verbose "Copying game set $($gameset.Name) from $($SourceDirectory.FullName) to $($gameDirectory)."
            foreach ( $variant in 'game_variants', 'map_variants' ) {
                $variantDirectory = Join-Path -Path $SourceDirectory -ChildPath $variant

                Write-Verbose "Searching for $variant $($gameset.$variant) in $variantDirectory."
                Get-ChildItem -Path $variantDirectory | Where-Object BaseName -In $gameSet.$variant | ForEach-Object {
                    $targetDirectory = Join-Path -Path $gameDirectory -ChildPath $variant
                    $targetPath = Join-Path -Path $targetDirectory -ChildPath $_.Name

                    Write-Verbose "Establishing symbolic link from target $($_.FullName) to path $targetPath."
                    New-Item -Target $_.FullName -Path $targetPath -ItemType SymbolicLink
                }
            }
        }
    }
}
