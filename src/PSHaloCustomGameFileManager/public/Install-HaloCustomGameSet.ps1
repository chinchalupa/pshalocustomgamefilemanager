<#
.SYNOPSIS
Installs halo reach custom games to the appropriate halo directory.

.DESCRIPTION
Installs halo reach custom games to the appropriate halo directory. Uses a
symbolic link to link files in the custom directory to avoid file duplication.

.PARAMETER HaloRootDirectory
The root directory of the Halo Master Chief Collection in the Steam directory.

.PARAMETER SourceDirectory
The directory containing all of the game variants and map variants.

.PARAMETER GameSet
The game set to install.

.EXAMPLE
> $haloRootDirectory = "D:\SteamLibrary\steamapps\common\Halo The Master Chief Collection"
> $sourceDirectory = "<your install directory here>"
> Install-HaloCustomGameSet -HaloRootDirectory $haloRootDirectory -SourceDirectory $sourceDirectory

Installs the halo custom game set alongside any existing files.
#>
function Install-HaloCustomGameSet {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [ValidateScript({ $_ | Test-Path })]
        [System.IO.DirectoryInfo]
        $HaloRootDirectory,

        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [ValidateScript({ $_ | Test-Path })]
        [System.IO.DirectoryInfo]
        $SourceDirectory,

        [Parameter(Mandatory, ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [ValidateNotNullOrEmpty()]
        [String]
        $GameSet
    )
    PROCESS {
        Get-HaloCustomGameSet -GameSet $GameSet | Sync-HaloCustomGameDirectory -HaloRootDirectory $HaloRootDirectory -SourceDirectory $SourceDirectory
    }
}
