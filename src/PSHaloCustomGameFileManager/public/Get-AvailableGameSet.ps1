<#
.SYNOPSIS
Returns the list of available game sets.

.PARAMETER GameSet
The specific game set to search for. Can also use regex.

.EXAMPLE
> Get-AvailableGameSet

Returns all game sets.

.EXAMPLE
> Get-AvailableGameSet -GameSet 'Jenga'

Returns all game sets matching the name 'Jenga'.
#>
function Get-AvailableGameSet {
    [CmdletBinding()]
    param (
        [Parameter(ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [String]
        $GameSet = '*'
    )
    PROCESS {
        @(
            'Jenga'
        ) | Where-Object Name -Like $GameSet
    }
}