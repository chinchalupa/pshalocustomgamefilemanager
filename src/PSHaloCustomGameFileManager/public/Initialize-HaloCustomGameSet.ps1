<#
.SYNOPSIS
Uninstalls all currently installed games and maps, then installs halo reach
custom games to the appropriate halo directory.

.DESCRIPTION
Installs halo reach custom games to the appropriate halo directory. Uses a
symbolic link to link files in the custom directory to avoid file duplication.

.PARAMETER HaloRootDirectory
The root directory of the Halo Master Chief Collection in the Steam directory.

.PARAMETER SourceDirectory
The directory containing all of the game variants and map variants.

.PARAMETER GameSet
The game set to install.

.EXAMPLE
> $haloRootDirectory = "D:\SteamLibrary\steamapps\common\Halo The Master Chief Collection"
> $sourceDirectory = "<your install directory here>"
> Initialize-HaloCustomGameSet -HaloRootDirectory $haloRootDirectory -SourceDirectory $sourceDirectory

Initializes the halo custom game set by removing all existing game and map
variants from the source directory.
#>
function Initialize-HaloCustomGameSet {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        [Parameter(ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [ValidateNotNullOrEmpty()]
        [HaloCustomGameSet]
        $GameSets = (Get-HaloCustomGameSet),

        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [ValidateScript({ $_ | Test-Path })]
        [System.IO.DirectoryInfo]
        $HaloRootDirectory,

        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [ValidateScript({ $_ | Test-Path })]
        [System.IO.DirectoryInfo]
        $SourceDirectory
    )
    PROCESS {
        Uninstall-HaloCustomGameSet -HaloRootDirectory $HaloRootDirectory
        Install-HaloCustomGameSet @PSBoundParameters
    }
}
