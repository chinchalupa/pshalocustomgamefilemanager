<#
.SYNOPSIS
Uninstalls all Halo custom game maps and variants from the root halo directory.

.PARAMETER HaloRootDirectory
The root directory for the Halo Master Chief Collection steamapp.

.EXAMPLE
> $haloRootDirectory = Get-Item "C:\SteamLibrary\steamapps\common\Halo The Master Chief Collection"
> Uninstall-HaloCustomGameSet -HaloRootDirectory $haloRootDirectory

Uninstalls all modules for all Halo game types at the directory location.
#>
function Uninstall-HaloCustomGameSet {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [ValidateScript({ $_ | Test-Path })]
        [System.IO.DirectoryInfo]
        $HaloRootDirectory
    )
    PROCESS {
        $gameDirectory = Join-Path -Path $HaloRootDirectory -ChildPath 'haloreach'
        foreach ( $variant in 'game_variants', 'map_variants' ) {
            $variantDirectory = Join-Path -Path $gameDirectory -ChildPath $variant
            Write-Verbose "Removing all items from $variantDirectory."
            Get-ChildItem $variantDirectory | Remove-Item -Force
        }
    }
}
