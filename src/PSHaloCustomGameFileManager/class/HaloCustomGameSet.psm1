<#
.SYNOPSIS
Class maps the JSON object to a class used as a loose data struct.
#>
class HaloCustomGameSet {
    [String]$Name
    [String]$halo_game
    [PSCustomObject] $game_variants
    [PSCustomObject]$map_variants

    constructor([String]$Name, [String]$halo_game, [PSCustomObject] $game_variants, [PSCustomObject]$map_variants) {
        $this.Name = $Name
        $this.halo_game = $halo_game
        $this.game_variants = $game_variants
        $this.map_variants = $map_variants
    }
}
